package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "tbDolar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDolar.findAll", query = "SELECT t FROM TbDolar t"),
    @NamedQuery(name = "TbDolar.findById", query = "SELECT t FROM TbDolar t WHERE t.id = :id"),
    @NamedQuery(name = "TbDolar.findByFechaConsulta", query = "SELECT t FROM TbDolar t WHERE t.fechaConsulta = :fechaConsulta"),
    @NamedQuery(name = "TbDolar.findByFechaDolar", query = "SELECT t FROM TbDolar t WHERE t.fechaDolar = :fechaDolar"),
    @NamedQuery(name = "TbDolar.findByValorDolar", query = "SELECT t FROM TbDolar t WHERE t.valorDolar = :valorDolar")})
public class TbDolar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "fecha_consulta")
    private String fechaConsulta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "fecha_dolar")
    private String fechaDolar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "valor_dolar")
    private String valorDolar;

    public TbDolar() {
    }

    public TbDolar(Integer id) {
        this.id = id;
    }

    public TbDolar(Integer id, String fechaConsulta, String fechaDolar, String valorDolar) {
        this.id = id;
        this.fechaConsulta = fechaConsulta;
        this.fechaDolar = fechaDolar;
        this.valorDolar = valorDolar;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(String fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public String getFechaDolar() {
        return fechaDolar;
    }

    public void setFechaDolar(String fechaDolar) {
        this.fechaDolar = fechaDolar;
    }

    public String getValorDolar() {
        return valorDolar;
    }

    public void setValorDolar(String valorDolar) {
        this.valorDolar = valorDolar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDolar)) {
            return false;
        }
        TbDolar other = (TbDolar) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.TbDolar[ id=" + id + " ]";
    }
    
}
