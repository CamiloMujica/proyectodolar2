package root.services;

import java.io.StringReader;
//import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
/*import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.POST;
import root.persistence.entities.TbDolar;*/



@Path("/")
public class Dolar {
    /* EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU_dolar");
    EntityManager em;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarConsultas() {
        em = emf.createEntityManager();
        List<TbDolar> consulta = em.createNamedQuery("TblDolar.findAll").getResultList();
        return Response.ok(200).entity(consulta).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String agregarConsulta(TbDolar consultaNueva){
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(consultaNueva);
        em.getTransaction().commit();
        return "Consulta guardada";
    } */
    
    @GET
    @Path("/fecha")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public String getFechaQuery(@QueryParam("dia") Integer dia, @QueryParam("mes") Integer mes, @QueryParam("anio") Integer anio) {

        String respuesta;
        String url;
        Response resp;
        boolean respu = true;

        String apiKey = "2af39f8def11ecee3f678885fd42a8a1a5b305fe";

        Client client = ClientBuilder.newClient();

        do {
            url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/"
                    + anio + "/" + mes + "/dias/" + dia + "/?formato=JSON&apikey=" + apiKey;
            resp = client.target(url).request().get();
            if (resp.getStatus() == 200) {
                respu = false;
            }
            dia--;
        } while (respu);
        {
            respuesta = resp.readEntity(String.class);
        }

        JsonReader j = Json.createReader(new StringReader(respuesta));
        JsonObject jobj = j.readObject();

        JsonArray subJsonArray = jobj.getJsonArray("Dolares");
        JsonObject jsonExtraido = subJsonArray.getJsonObject(0);

        String valorDelDolar = jsonExtraido.getString("Valor");
        String fechaDelDolar = jsonExtraido.getString("Fecha");

        JsonObject jr = Json.createObjectBuilder()
                .add("Fuente", "SBIF")
                .add("Valor Dolar", jsonExtraido.getString("Valor"))
                .add("Fecha", jsonExtraido.getString("Fecha"))
                .build();
        
       /* TbDolar consulta = new TbDolar(dia, fechaDelDolar, fechaDelDolar, valorDelDolar);
        consulta.setFechaConsulta(anio+"-"+mes+"-"+dia);
        consulta.setFechaDolar(jsonExtraido.getString("Fecha"));
        consulta.setValorDolar(jsonExtraido.getString("Valor"));
        Dolar insertar = new Dolar();
        insertar.agregarConsulta(consulta); */
        

        return jr.toString();
        
        
    }
    
    
   
}
