package root.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-12-08T18:09:32")
@StaticMetamodel(TbDolar.class)
public class TbDolar_ { 

    public static volatile SingularAttribute<TbDolar, String> fechaConsulta;
    public static volatile SingularAttribute<TbDolar, String> fechaDolar;
    public static volatile SingularAttribute<TbDolar, Integer> id;
    public static volatile SingularAttribute<TbDolar, String> valorDolar;

}